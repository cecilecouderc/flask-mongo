from flask import Flask, render_template, request, app
import db

app = Flask(__name__)

if __name__ == '__main__':
    app.run(port=8000)

@app.route('/')
@app.route('/<name>')
def home(name=None):
    return render_template('home.html', name=name)

@app.route("/test")
def test():
    db.db.collection.insert_one({"name": "John"})
    return "Connected to the data base!"

