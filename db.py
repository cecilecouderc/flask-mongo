from flask import Flask
from flask_pymongo import pymongo
from pymongo import MongoClient
from app import app

CONNECTION_STRING = "mongodb+srv://cecile2:cecile2@cc-e23vq.mongodb.net/test?retryWrites=true&w=majority"
client = pymongo.MongoClient(CONNECTION_STRING)
db = client.get_database('flask_mongodb_atlas')
user_collection = pymongo.collection.Collection(db, 'user_collection')

